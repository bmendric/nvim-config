-- I hold shift too much
vim.api.nvim_create_user_command("Q", "quit", {})
vim.api.nvim_create_user_command("W", "write", {})
vim.api.nvim_create_user_command("WQ", "wq", {})
vim.api.nvim_create_user_command("Wq", "wq", {})

-- make == task
vim.api.nvim_create_user_command("Task", "make", {})

-- I also can't type good
vim.fn.execute("Abolish teh the")
vim.fn.execute("Abolish delimete{,s} delimiter{}")
vim.fn.execute("Abolish lastest latest")
vim.fn.execute("Abolish gaurantee guarantee")
vim.fn.execute("Abolish persistan{ce,t,tly} persisten{}")
vim.fn.execute("Abolish seperat{e,es,ed,ing,ely,ion,ions,or} separat{}")

-- ¯\_(ツ)_/¯
vim.cmd([[iabbrev shrug,, ¯\_(ツ)_/¯]])
vim.cmd([[iabbrev table,, (╯°□°）╯︵ ┻━┻<del>]]) -- autopairs is being too smart
vim.cmd([[iabbrev wat,, ಠ_ಠ]])
