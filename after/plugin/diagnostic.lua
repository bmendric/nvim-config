vim.diagnostic.config({
  virtual_text = {
    source = "if_many",
  },
  severity_sort = true,
  update_in_insert = true,
})
