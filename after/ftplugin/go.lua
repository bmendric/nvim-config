vim.opt_local.expandtab = false

if (vim.loop.fs_stat("Makefile") or {}).type == "file" then
  vim.opt.makeprg = "make"
elseif (vim.loop.fs_stat("Taskfile.yaml") or {}).type == "file" then
  vim.opt.makeprg = "task"
else
  vim.opt.makeprg = "go"
end
