vim.filetype.add({
  extension = {
    tf = 'terraform',
    tfvars = 'terraform',
    tfstate = 'json',
    hcl = 'hcl',
    wiki = 'markdown',
  },
  filename = {
    ['go.mod'] = 'gomod',
    ['go.sum'] = 'gosum',
    ['terraform.rc'] = 'hcl',
    ['.terraformrc'] = 'hcl',
  },
  pattern = {
    ['.*%.tfstate%.backup'] = 'json',
    ['.*%.tftest%.hcl'] = 'hcl',
    ['.*%.tftest%.json'] = 'json',
  },
})
