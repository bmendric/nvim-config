-- Ignore compile files
vim.opt.wildignore = "__pycache__"
vim.opt.wildignore:append({ "*.o", "*~", "*.pyc", "*pycache*" })

-- Pretty >o__.
vim.opt.termguicolors = true

-- Floating popup menu thingy for completions
vim.opt.completeopt = "menu,menuone,noinsert"
-- vim.opt.pumblend = 17
vim.opt.wildmode = "longest:full"

-- Search options
vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.inccommand = "split"

-- Splits
vim.opt.splitright = true
vim.opt.splitbelow = true

-- Cursorline highlighting
-- 	On, but only for the active buffer
vim.opt.cursorline = true
local group = vim.api.nvim_create_augroup("CursorLineControl", { clear = true })
local set_cursorline = function(event, value, pattern)
  vim.api.nvim_create_autocmd(event, {
    group = group,
    pattern = pattern,
    callback = function()
      vim.opt_local.cursorline = value
    end,
  })
end
set_cursorline("WinLeave", false)
set_cursorline("WinEnter", true)
set_cursorline("FileType", false, "TelescopePrompt")

-- Tabs
vim.opt.cindent = true

vim.opt.tabstop = 2
vim.opt.shiftwidth = 0 -- defer to whatever tabstop is set to
vim.opt.softtabstop = -1 -- defer to whatever shiftwidth is set to
vim.opt.expandtab = true

-- Line Wrapping
vim.opt.linebreak = true -- wrap lines intelligently instead of mid-word
vim.opt.breakindent = true -- make line wraps match indentation
vim.opt.showbreak = string.rep("+", 3) -- display wrapped lines prefix with pluses

-- Formatting
vim.opt.formatoptions = vim.opt.formatoptions
  - "t" -- don't format code to textwidth
  + "n" -- indent past the formatlistpat

-- Grep so hard
if vim.fn.executable("rg") == 1 then
  vim.opt.grepprg = "rg --vimgrep"
end

-- Misc
vim.opt.backspace = "indent,eol,start"
vim.opt.clipboard = "unnamedplus" -- this seems better but I don't entirely understand why
vim.opt.mouse = "" -- mouse bad; am cat
vim.opt.number = true -- show line numbers
vim.opt.scrolloff = 10
vim.opt.signcolumn = "yes"
vim.opt.spell = true

-- updating options with hiddenoff and algorithm:minimal, should be better diff
-- buffer handling and a better algorithm for calculating minimal diffs,
-- respectively
vim.opt.diffopt = { "internal", "filler", "closeoff", "hiddenoff", "algorithm:minimal" }
