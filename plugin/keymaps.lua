-- Make movement more intelligent when traversing multiple lines, wrapped
-- lines, or whitespace
local intelli_move = function(direction)
  return function()
    local jump_count = vim.v.count
    if jump_count == 0 then
      vim.fn.execute(vim.fn.printf("normal! g%s", direction))
      return
    end

    if jump_count > 5 then
      vim.fn.execute("normal! m'")
    end

    vim.fn.execute(vim.fn.printf("normal! %d%s", jump_count, direction))
  end
end
vim.keymap.set("n", "j", intelli_move("j"), { noremap = true, silent = true })
vim.keymap.set("n", "k", intelli_move("k"), { noremap = true, silent = true })

local intelli_jump = function(direction)
  return function()
    local before = vim.opt.lazyredraw
    vim.opt.lazyredraw = true

    col = vim.fn.virtcol(".")
    if direction == "j" then
      -- search downwards, in the given virtual column, for \S (whitespace)
      vim.fn.execute(vim.fn.printf("/\\%%%sv\\S", col))
    elseif direction == "k" then
      -- search upwards, in the given virtual column, for \S (whitespace)
      vim.fn.execute(vim.fn.printf("?\\%%%sv\\S", col))
    end

    vim.cmd("nohlsearch")
    vim.opt.lazyredraw = before
  end
end
vim.keymap.set("n", "gj", intelli_jump("j"), { noremap = true, silent = true })
vim.keymap.set("n", "gk", intelli_jump("k"), { noremap = true, silent = true })

-- Run the last command
-- TODO this is interesting, but it doesn't seem to want to redraw the commandline
vim.keymap.set("n", "<leader><leader>c", ":<up>", { noremap = true, silent = true })

-- Movement between splits
vim.keymap.set("n", "<C-j>", "<C-w><C-j>", { noremap = true, silent = true })
vim.keymap.set("n", "<C-k>", "<C-w><C-k>", { noremap = true, silent = true })
vim.keymap.set("n", "<C-l>", "<C-w><C-l>", { noremap = true, silent = true })
vim.keymap.set("n", "<C-h>", "<C-w><C-h>", { noremap = true, silent = true })

-- Clear hlsearch after doing a search, otherwise act casual
-- TODO unclear why my lua version of this didn't work
-- alternative?
vim.keymap.set("n", "<esc>", "<cmd>nohlsearch<CR><esc>", { noremap = true, silent = true })
-- vim.keymap.set(
--   'n', '<CR>',
--   [[ {-> v:hlsearch ? "<cmd>nohlsearch\<CR>" : "\<CR>"}() ]],
--   { replace_keycodes = false, expr = true, noremap = true, silent = true }
-- )

-- Quickfix
vim.keymap.set("n", "<leader>co", function() vim.cmd.copen(); vim.cmd.wincmd('p') end, { noremap = true, silent = true })
vim.keymap.set("n", "<leader>cO", function() vim.cmd.copen() end, { noremap = true, silent = true })
vim.keymap.set("n", "<leader>cc", function() vim.cmd.cclose() end, { noremap = true, silent = true })

-- Diagnostics
-- TODO how are diagnostics useful?
vim.keymap.set("n", "<leader>dn", function()
  vim.diagnostic.goto_next()
end, { noremap = true, silent = true })
vim.keymap.set("n", "<leader>dp", function()
  vim.diagnostic.goto_prev()
end, { noremap = true, silent = true })
vim.keymap.set("n", "<leader>dh", function()
  vim.diagnostic.open_float()
end, { noremap = true, silent = true })

-- Tabs
vim.keymap.set("n", "<leader>tc", function() vim.cmd.tabclose() end, { noremap = true, silent = true })
