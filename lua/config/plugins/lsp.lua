return {
  {
    "j-hui/fidget.nvim",
    tag = "legacy",
    opts = {
      text = {
        spinner = "dots",
      },
    },
  },
  {
    "nvimtools/none-ls.nvim",
    dependencies = { "nvim-lua/plenary.nvim" },
    event = { "BufReadPre", "BufNewFile" },
    opts = function()
      local null_ls = require("null-ls")
      local sources = {
        null_ls.builtins.formatting.trim_newlines,
        null_ls.builtins.formatting.trim_whitespace,
      }

      local check_sources = {
        ["golangci-lint"] = { null_ls.builtins.diagnostics.golangci_lint },
        mypy = { null_ls.builtins.diagnostics.mypy },
        ["puppet-lint"] = { null_ls.builtins.diagnostics.puppet_lint },
        pylint = { null_ls.builtins.diagnostics.pylint },
        rubocop = { null_ls.builtins.diagnostics.rubocop },
        yamllint = { null_ls.builtins.diagnostics.yamllint },
        jq = { null_ls.builtins.formatting.jq },
        terraform = { null_ls.builtins.formatting.terraform_fmt },
      }

      for exe, source in pairs(check_sources) do
        if vim.fn.executable(exe) == 1 then
          for _, v in ipairs(source) do
            table.insert(sources, v)
          end
        end
      end

      return { sources = sources }
    end,
  },
  {
    "neovim/nvim-lspconfig",
    dependencies = { "hrsh7th/cmp-nvim-lsp" },
    event = { "BufReadPre", "BufNewFile" },
    config = function(_, opts)
      lspconfig = require("lspconfig")

      -- TODO handler overrides, particularly for window/showMessage

      -- TODO figure out why TJ has brought this into existence
      local custom_init = function(client)
        client.config.flags = client.config.flags or {}
        client.config.flags.allow_incremental_sync = true
      end

      local augroup_format = vim.api.nvim_create_augroup("CustomLspFormat", { clear = true })
      local augroup_highlight = vim.api.nvim_create_augroup("CustomLspHighlight", { clear = true })

      local custom_attach = function(client, bufnr)
        vim.keymap.set("i", "<C-s>", vim.lsp.buf.signature_help, { buffer = 0, noremap = true, silent = true })
        vim.keymap.set("n", "<leader>cs", vim.lsp.buf.rename, { buffer = 0, noremap = true, silent = true })
        vim.keymap.set("n", "<leader>ca", vim.lsp.buf.code_action, { buffer = 0, noremap = true, silent = true })
        vim.keymap.set("n", "gd", vim.lsp.buf.definition, { buffer = 0, noremap = true, silent = true })
        vim.keymap.set("n", "gD", vim.lsp.buf.declaration, { buffer = 0, noremap = true, silent = true })
        vim.keymap.set("n", "gf", vim.lsp.buf.type_definition, { buffer = 0, noremap = true, silent = true })
        vim.keymap.set("n", "gr", require("telescope.builtin").lsp_references, { buffer = 0, noremap = true, silent = true })
        vim.keymap.set("n", "gI", require("telescope.builtin").lsp_implementations, { buffer = 0, noremap = true, silent = true })
        vim.keymap.set("n", "K", vim.lsp.buf.hover, { buffer = 0, noremap = true, silent = true })

        if client.server_capabilities.documentFormattingProvider then
          vim.api.nvim_clear_autocmds({ group = augroup_format, buffer = bufnr })
          vim.api.nvim_create_autocmd("BufWritePre", {
            group = augroup_format,
            buffer = bufnr,
            callback = function()
              vim.lsp.buf.format({ async = false, bufnr = bufnr })
            end,
          })
        end

        if client.server_capabilities.documentHighlightProvider then
          vim.api.nvim_clear_autocmds({ group = augroup_highlight, buffer = bufnr })
          vim.api.nvim_create_autocmd("CursorHold", {
            group = augroup_highlight,
            buffer = bufnr,
            callback = function()
              vim.lsp.buf.document_highlight()
            end,
          })
          vim.api.nvim_create_autocmd("CursorMoved", {
            group = augroup_highlight,
            buffer = bufnr,
            callback = function()
              vim.lsp.buf.clear_references()
            end,
          })
        end
      end

      local updated_capabilities = vim.tbl_deep_extend(
        "force",
        vim.lsp.protocol.make_client_capabilities(),
        require("cmp_nvim_lsp").default_capabilities()
      )
      -- TODO I believe this is being set to make sure cmp with (default insert
      -- mode) plays nice with everything?
      updated_capabilities.textDocument.completion.completionItem.insertReplaceSupport = false

      local servers = {
        { name = "gopls", config = true },
        { name = "jedi_language_server", exe = "jedi-language-server", config = true },
        { name = "terraformls", config = true },
        { name = "tflint", config = true },
      }

      local setup_server = function(server)
        exe = server.exe or server.name

        if (not server.config) or vim.fn.executable(exe) ~= 1 then
          return
        end

        if type(server.config) ~= "table" then
          server.config = {}
        end

        config = vim.tbl_deep_extend("force", {
          on_init = custom_init,
          on_attach = custom_attach,
          capabilities = updated_capabilities,
        }, server.config)

        lspconfig[server.name].setup(config)
      end

      for _, server in ipairs(servers) do
        setup_server(server)
      end
    end,
  },
  {
    "ray-x/lsp_signature.nvim",
    event = "VeryLazy",
    opts = {
      bind = true,
      padding = ' ',
    },
  }
}
