return {
  {
    "hashivim/vim-terraform",
    lazy = true,
    ft = { "terraform", "hcl", },
  },
  {
    "rodjek/vim-puppet",
    lazy = true,
    ft = { "puppet", },
  },
}
