return {
  { "nvim-lua/plenary.nvim", lazy = true },
  { "tpope/vim-repeat", event = "VeryLazy" },
  { "tpope/vim-surround", event = "VeryLazy" },
  { "tpope/vim-unimpaired", event = "VeryLazy" },
  { "tpope/vim-dispatch", event = "VeryLazy" },
  { "tpope/vim-abolish" },

  -- tpope getting kicked out for something else?!?! (╯°□°）╯︵ ┻━┻
  -- { "tpope/vim-commentary", event = "VeryLazy" },
  {
    "numToStr/Comment.nvim",
    event = "VeryLazy",
    opts = {
      ignore = "^$",
    },
  },
}
