return {
  {
    "rcarriga/nvim-notify",
    lazy = false,
    dependencies = { "nvim-lua/plenary.nvim" },
    keys = {
      {
        "<leader>nd",
        function()
          require("notify").dismiss({ silent = true, pending = true })
        end,
        desc = "Dismiss all notifications",
      },
    },
    opts = {
      render = "minimal",
    },
    config = function(_, opts)
      vim.notify = function(msg, level, opts)
        require("notify")(msg, level, opts)
      end
    end,
  },
}
