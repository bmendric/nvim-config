return {
  { "tpope/vim-fugitive", cmd = {"Git", "G",} },
  {
    "harrisoncramer/gitlab.nvim",
    dependencies = {
      "MunifTanjim/nui.nvim",
      "nvim-lua/plenary.nvim",
      "sindrets/diffview.nvim",
      "stevearc/dressing.nvim",
      "nvim-tree/nvim-web-devicons",
    },
    enabled = true,
    build = function() require("gitlab.server").build(true) end,
    config = function()
      local gitlab = require("gitlab")
      local gitlab_server = require("gitlab.server")
      gitlab.setup({
        config_path = vim.fn.getenv("HOME"),
        create_mr = {
          target = "main",
        },
        discussion_tree = {
          position = "bottom",
          toggle_node = "<Enter>",
          tree_type = "by_file_name",
        },
        info = {
          horizontal = true,
        },
        merge = {
          delete_branch = true,
        },
      })
      vim.keymap.set("n", "<leader>gr", gitlab.review)
      vim.keymap.set("n", "<leader>gs", gitlab.summary)
      vim.keymap.set("n", "<leader>gA", gitlab.approve)
      vim.keymap.set("n", "<leader>gR", gitlab.revoke)
      vim.keymap.set("n", "<leader>gc", gitlab.create_comment)
      vim.keymap.set("v", "<leader>gc", gitlab.create_multiline_comment)
      vim.keymap.set("v", "<leader>gC", gitlab.create_comment_suggestion)
      vim.keymap.set("n", "<leader>gO", gitlab.create_mr)
      vim.keymap.set("n", "<leader>gm", gitlab.move_to_discussion_tree_from_diagnostic)
      vim.keymap.set("n", "<leader>gn", gitlab.create_note)
      vim.keymap.set("n", "<leader>gd", gitlab.toggle_discussions)
      vim.keymap.set("n", "<leader>gaa", gitlab.add_assignee)
      vim.keymap.set("n", "<leader>gad", gitlab.delete_assignee)
      vim.keymap.set("n", "<leader>gra", gitlab.add_reviewer)
      vim.keymap.set("n", "<leader>grd", gitlab.delete_reviewer)
      vim.keymap.set("n", "<leader>gp", gitlab.pipeline)
      vim.keymap.set("n", "<leader>go", gitlab.open_in_browser)
      vim.keymap.set("n", "<leader>gM", gitlab.merge)
    end,
  },
}
