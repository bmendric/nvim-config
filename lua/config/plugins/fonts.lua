return {
  { "nvim-tree/nvim-web-devicons", lazy = true },
  {
    "lewis6991/gitsigns.nvim",
    lazy = true,
    event = { "BufReadPre", "BufNewFile", },
    config = true,
  },
}
