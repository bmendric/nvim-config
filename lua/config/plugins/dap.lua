return {
  {
    "rcarriga/nvim-dap-ui",
    opts = {
      layouts = {
        {
          elements = {
            { id = "scopes", size = 0.4 },
            { id = "console", size = 0.3 },
            { id = "repl", size = 0.3 }
          },
          position = "bottom",
          size = 15
        },
        {
          elements = {
            { id = "stacks", size = 0.6 },
            { id = "breakpoints", size = 0.2 },
            { id = "watches", size = 0.2 },
          },
          position = "left",
          size = 40
        },
      },
    },
    config = function(_, opts)
      local dap = require("dap")
      local dapui = require("dapui")
      dapui.setup(opts)

      local original = {}
      local debug_map = function(lhs, rhs, opts)
        local keymaps = vim.api.nvim_get_keymap("n")
        original[lhs] = vim.tbl_filter(function(v)
          return v.lhs == lhs
        end, keymaps)[1] or true

        local merged_opts = vim.tbl_deep_extend("force", {
          noremap = true,
          silent = true,
        }, opts)

        vim.keymap.set("n", lhs, rhs, merged_opts)
      end

      local debug_unmap = function()
        for k, v in pairs(original) do
          if v == true then
            vim.keymap.del("n", k)
          else
            local rhs = v.rhs

            v.lhs = nil
            v.rhs = nil
            v.buffer = nil
            v.mode = nil
            v.sid = nil
            v.lnum = nil

            vim.keymap.set("n", k, rhs, v)
          end
        end

        original = {}
      end

      vim.keymap.set("n", "<leader>db", dap.toggle_breakpoint, { noremap = true, silent = true })
      vim.keymap.set("n", "<leader>dB", function() dap.set_breakpoint(vim.fn.input("Condition: ")) end, { noremap = true, silent = true })
      vim.keymap.set("n", "<leader>dc", dap.run_to_cursor, { noremap = true, silent = true })
      vim.keymap.set("n", "<leader>dr", dap.repl.toggle, { noremap = true, silent = true })
      vim.keymap.set("n", "<leader>dq", dap.terminate, { noremap = true, silent = true })
      vim.keymap.set("n", "<leader>dt", function() dapui.toggle({ reset = true }) end, { noremap = true, silent = true })

      vim.keymap.set("n", "<M-c>", dap.continue, { noremap = true, silent = true })

      dap.listeners.after.event_initialized["dapui_config"] = function()
        debug_map("<M-s>", dap.step_into, {})
        debug_map("<M-o>", dap.step_out, {})
        debug_map("<M-n>", dap.step_over, {})
        debug_map("K", function() require("dap.ui.widgets").hover() end, {})

        dapui.open({})
      end

      dap.listeners.before.event_terminated["dapui_config"] = function()
        debug_unmap()

        dapui.close({})
      end

      dap.listeners.before.event_exited["dapui_config"] = function()
        dapui.close({})
      end
    end,
  },
  {
    "mfussenegger/nvim-dap",
    dependencies = {
      "rcarriga/nvim-dap-ui",
    },
  },
  {
    "leoluz/nvim-dap-go",
    opts = {},
  }
}
