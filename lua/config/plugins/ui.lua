return {
  {
    "RRethy/vim-illuminate",
    event = { "BufReadPost", "BufNewFile" },
    opts = {
      delay = 500,
      large_file_cutoff = 2000,
      large_file_overrides = {
        providers = { "lsp" },
      },
    },
    config = function(_, opts)
      require("illuminate").configure(opts)
    end,
  },
  {
    "lukas-reineke/indent-blankline.nvim",
    main = "ibl",
    opts = {
      scope = {
        enabled = false,
      },
    },
  },
  {
    "stevearc/dressing.nvim",
    opts = {},
  },
  {
    "stevearc/oil.nvim",
    event = "VeryLazy",
    opts = {
      default_file_explorer = true,
      columns = {
        "icon",
        "permissions",
        "size",
        "mtime"
      },
      keymaps = {
        -- Update vertical split default
        ["<C-s>"] = false,
        ["<C-v>"] = "actions.select_vsplit",
        
        -- Update horizontal split default
        ["<C-h>"] = false,
        ["<C-x>"] = "actions.select_split",

        -- idk who thought this was reasonable
        ["<C-l>"] = false,
        ["<C-r>"] = "actions.refresh",
      },
      view_options = {
        show_hidden = true,
      },
    },
    dependencies = { "nvim-tree/nvim-web-devicons", },
  },
  {
    "AckslD/messages.nvim",
    event = "VeryLazy",
    dependencies = {
      "MunifTanjim/nui.nvim",
    },
    opts = {
      prepare_buffer = function(opts)
        local Split = require("nui.split")
        local split = Split(opts)

        split:mount()

        split:map('n', 'q', function() split:unmount() end, { noremap = true })

        return split.winid
      end,

      buffer_opts = function(_)
        return {
          relative = 'editor',
          position = 'bottom',
          size = '25%',
        }
      end,

      post_open_float = function(winnr)
        local bufnr = vim.api.nvim_win_get_buf(winnr)
        vim.api.nvim_buf_set_option(bufnr, 'modifiable', false)
        vim.api.nvim_buf_set_option(bufnr, 'readonly', true)
        vim.api.nvim_buf_set_cursor(winnr, {vim.api.nvim_buf_line_count(bufnr), 0})
      end,
    },
  },
}
