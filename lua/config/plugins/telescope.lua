local get_find_command = function()
  local command = nil
  if vim.fn.executable("fdfind") == 1 then
    command = "fdfind"
  elseif vim.fn.executable("fd") == 1 then
    command = "fd"
  else
    return nil
  end

  -- return { command, "--strip-cwd-prefix", "--type", "f" } -- need to check for fd >= 8.3.0 for this to be a real option
  return { command, "--type", "f", }
end

return {
  {
    "nvim-telescope/telescope.nvim",
    branch = "0.1.x",
    event = "VeryLazy",
    dependencies = {
      "nvim-lua/plenary.nvim",
    },
    opts = {
      extensions = {
        file_browser = {
          -- hijack_netrw = true,
          hidden = true,
          respect_gitignore = false,
          grouped = true,
          sorting_strategy = "ascending",
          scroll_strategy = "cycle",
          layout_config = {
            prompt_position = "top",
          },
        },
      },
      pickers = {
        find_files = {
          find_command = get_find_command(),
        },
      },
    },
    config = function(_, opts)
      -- Handle config loading
      local telescope = require("telescope")

      telescope.setup(opts)

      -- Load plugins
      _ = telescope.load_extension("dap")
      _ = telescope.load_extension("notify")
      _ = telescope.load_extension("file_browser")
      _ = telescope.load_extension("fzf")

      -- Keymaps
      local telescope_builtin = require("telescope.builtin")
      vim.keymap.set(
        { "n", "t" },
        "<leader>/",
        telescope_builtin.current_buffer_fuzzy_find,
        { noremap = true, silent = true }
      )

      vim.keymap.set({ "n", "t" }, "<leader>fb", telescope_builtin.buffers, { noremap = true, silent = true })
      vim.keymap.set({ "n", "t" }, "<leader>fd", telescope_builtin.find_files, { noremap = true, silent = true })
      vim.keymap.set(
        { "n", "t" },
        "<leader>fe",
        telescope.extensions.file_browser.file_browser,
        { noremap = true, silent = true }
      )
      vim.keymap.set({ "n", "t" }, "<leader>fg", telescope_builtin.live_grep, { noremap = true, silent = true })
      vim.keymap.set({ "n", "t" }, "<leader>fh", telescope_builtin.help_tags, { noremap = true, silent = true })
      vim.keymap.set({ "n", "t" }, "<leader>ft", telescope_builtin.git_files, { noremap = true, silent = true })
    end,
  },
  {
    "nvim-telescope/telescope-fzf-native.nvim",
    dependencies = { "nvim-telescope/telescope.nvim" },
    build = "make",
    event = "VeryLazy",
  },
  {
    "nvim-telescope/telescope-dap.nvim",
    dependencies = { "nvim-telescope/telescope.nvim" },
    event = "VeryLazy",
  },
  {
    "nvim-telescope/telescope-file-browser.nvim",
    event = "VeryLazy",
    dependencies = {
      "nvim-telescope/telescope.nvim",
      "nvim-lua/plenary.nvim",
    },
  },
}
