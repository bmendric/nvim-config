return {
  { "windwp/nvim-autopairs", event = "InsertEnter", opts = {} },
  { "andymass/vim-matchup", lazy = false, },
  {
    "L3MON4D3/cmp-luasnip-choice",
    event = { "InsertEnter", "CmdlineEnter", "CmdwinEnter" },
    opts = { auto_open = true },
  },
  {
    "hrsh7th/nvim-cmp",
    version = false,
    event = "InsertEnter",
    dependencies = {
      "hrsh7th/cmp-nvim-lsp",
      "hrsh7th/cmp-buffer",
      "hrsh7th/cmp-path",
      "hrsh7th/cmp-cmdline",

      -- "saadparwaiz1/cmp_luasnip",
      "L3MON4D3/cmp-luasnip-choice",

      "nvim-tree/nvim-web-devicons",
      "windwp/nvim-autopairs",
    },
    config = function()
      vim.opt.shortmess:append("c")

      local cmp = require("cmp")
      local luasnip = require("luasnip")

      local has_words_before = function()
        unpack = unpack or table.unpack
        local line, col = unpack(vim.api.nvim_win_get_cursor(0))
        return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
      end

      cmp.setup({
        snippet = {
          expand = function(args)
            require("luasnip").lsp_expand(args.body)
          end,
        },
        formatting = {
          format = function(entry, vim_item)
            if vim.tbl_contains({ "path" }, entry.source.name) then
              local icon, hl_group = require("nvim-web-devicons").get_icon(entry:get_completion_item().label)
              if icon then
                vim_item.kind = icon
                vim_item.ken_hl_group = hl_group
              end
            end
            return vim_item
          end,
        },
        mapping = {
          ["<C-e>"] = cmp.mapping.abort(),
          ["<CR>"] = cmp.mapping.confirm({ select = true }),
          -- TODO understand why this causes an extra case of pressing enter when selecting completions
          -- ["<CR>"] = cmp.mapping({
          --   -- TODO is the intelligent version actually better?
          --   -- i = function(fallback)
          --   --   if cmp.visible() and cmp.get_active_entry() then
          --   --     cmp.confirm({ behavior = cmp.ConfirmBehavior.Replace, select = false })
          --   --   else
          --   --     fallback()
          --   --   end
          --   -- end,
          --   i = cmp.mapping.confirm({ behavior = cmp.ConfirmBehavior.Replace, select = true }),
          --   s = cmp.mapping.confirm({ select = true }),
          --   c = cmp.mapping.confirm({ behavior = cmp.ConfirmBehavior.Replace, select = true }),
          -- }),
          ["<Tab>"] = cmp.mapping(function(fallback)
            if cmp.visible() then
              cmp.select_next_item()
            elseif luasnip.expand_or_jumpable() then
              luasnip.expand_or_jump()
            elseif has_words_before() then
              cmp.complete()
            else
              fallback()
            end
          end, { "i", "s" }),
          ["<S-Tab>"] = cmp.mapping(function(fallback)
            if cmp.visible() then
              cmp.select_prev_item()
            elseif luasnip.jumpable(-1) then
              luasnip.jump(-1)
            else
              fallback()
            end
          end, { "i", "s" }),
        },
        sorting = {
          comparators = {
            cmp.config.compare.offset,
            cmp.config.compare.exact,
            cmp.config.compare.score,
            function(a, b)
              local a_under = select(2, a.completion_item.label:find("^_+")) or 0
              local b_under = select(2, b.completion_item.label:find("^_+")) or 0
              if a_under == b_under then
                return nil
              end
              return a_under < b_under
            end,
            cmp.config.compare.kind,
            cmp.config.compare.sort_text,
            cmp.config.compare.length,
            cmp.config.compare.order,
          },
        },
        sources = cmp.config.sources({
          { name = "nvim_lsp" },
          { name = "treesitter" },
          { name = "luasnip_choice" },
        }, {
          { name = "path" },
          { name = "buffer", keyword_length = 5 },
        }),
        preselect = cmp.PreselectMode.None,
        -- view = {
        --   entries = "native"
        -- },
        window = {
          completion = {
            scrolloff = 2,
          },
        },
        experimental = {
          ghost_text = { hl_group = "Comment" },
        },
      })

      local cmdline_mappings = {
        ["<Tab>"] = {
          c = function()
            if cmp.visible() then
              cmp.select_next_item()
            else
              cmp.complete()
            end
          end,
        },
        ["<S-Tab>"] = {
          c = function()
            if cmp.visible() then
              cmp.select_prev_item()
            else
              cmp.complete()
            end
          end,
        },
      }

      cmp.setup.cmdline({ "/", "?" }, {
        mapping = cmdline_mappings,
        sources = {
          { name = "buffer" },
        },
      })

      cmp.setup.cmdline({ ":" }, {
        mapping = cmdline_mappings,
        sources = cmp.config.sources({
          { name = "path" },
        }, {
          {
            name = "cmdline",
            max_item_count = 20,
          },
        }),
      })
    end,
  },
}
