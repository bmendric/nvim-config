return {
  {
    "nvim-treesitter/nvim-treesitter",
    version = false,
    build = ":TSUpdate",
    event = { "BufReadPost", "BufNewFile" },
    cmd = { "TSUpdate", "TSUpdateSync" },
    dependencies = {
      "nvim-treesitter/nvim-treesitter-textobjects",
      "nvim-treesitter/nvim-treesitter-context",
    },
    main = "nvim-treesitter.configs",
    opts = {
      -- ensure_installed = {
      --   "bash",
      --   "go",
      --   "html",
      --   "javascript",
      --   "json",
      --   "lua",
      --   "luadoc",
      --   "luap",
      --   "markdown",
      --   "markdown_inline",
      --   "python",
      --   "regex",
      --   "vim",
      --   "vimdoc",
      --   "yaml",
      -- },
      -- auto_install = false,

      highlight = { enabled = true },
      textobjects = {
        select = {
          enable = true,
          keymaps = {
            ["af"] = "@function.outer",
            ["if"] = "@function.inner",
            ["ac"] = "@class.outer",
            ["ic"] = "@class.inner",
          },
        },
      },
      matchup = {
	      enable = true,
	      disable_virtual_text = true,
      },
    },
  },
}
