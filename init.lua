-- Load things fast-like
-- TODO should lazy's cache be disabled because this is turned on?
vim.loader.enable()

-- Map leader early so everything gets the right leader
vim.g.mapleader = ","
vim.g.maplocalleader = ","

-- Load globals
require("config.globals")

-- Install lazy.nvim if it doesn't already exist
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({ "git", "clone", "--filter=blob:none", "https://github.com/folke/lazy.nvim.git", lazypath })
  vim.fn.system({ "git", "-C", lazypath, "checkout", "tags/stable" })
end
vim.opt.runtimepath:prepend(lazypath)

-- Trigger lazy to do all the things
require("lazy").setup("config.plugins", {
  ui = {
    border = "rounded",
    icons = {
      cmd = "⌘",
      config = "🛠",
      event = "📅",
      ft = "📂",
      init = "⚙",
      keys = "🗝",
      plugin = "🔌",
      runtime = "💻",
      source = "📄",
      start = "🚀",
      task = "📌",
    },
  },
  install = {
    colorscheme = { "moonfly" },
  },
  performance = {
    rtp = {
      disabled_plugins = {
        "matchit",
        "matchparen",
        "netrwPlugin",
        "tohtml",
        "tutor",
      },
    },
  },
})
